var app = angular.module('testApp',['ui.router']);    

app.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/');
	$urlRouterProvider.otherwise('/404');

	$stateProvider
	    
	    .state('home', {
	      url: "/",
	      templateUrl: "home.html"
	    }) 
	    
	    .state('about', {
	      url: "/about",
	      abstract: true,
	      template: '<ui-view/>'
	    }) 
	    .state('about.index', {
	      url: "",
	      templateUrl: 'about.html'
	    })
	    .state('about.card', {
	      url: "/card",
	      templateUrl: 'singlecard.html'
	    })
	    .state('error', {
	      url: "/404",
	      template: '<div class="container">Error 404<br>Sorry. This page does not exists</div>'
	    })
	    
	    .state('contact', {
	      url: "/contact",
	      templateUrl: "contact.html"
	    });
	});

//---------------directive-----------
app.directive('modalDialog', function() {
	  return {
	  	restrict: 'E',
	    templateUrl: 'modal.html'
	  };
});

//---------------service-----------
app.service('validationService', function() {	           
	this.isValid = function(value, pattern) {
		return pattern.test(value);
	}
});

// ---------------------factory----------------------
app.factory('cardFactory', function(){
	var factory = {};

	var cardlist=[{
            title:'Lorem ipsum.',
            description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error ratione distinctio qui quaerat adipisci odio assumenda, officia animi fuga quia, atque!',
            },{
            title:'Sed ut perspiciatis',
            description:'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto'
            },{
            title:'At vero eos',
            description:'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?'
            },{
            title:'Excepteur sint',
            description:'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore'
     }];

	

	factory.getCards = function(){
		return cardlist;
	};

	factory.getSingleCard = function(index){
		return cardlist[index];
	};

	return factory;

});
//-------------------------component--------------
app.component("navbar",{
      templateUrl: 'navbar.html',
      controller: function () {
	    function confirmUserName() {
	      this.userNameConfirmed = true;
	    }

	    this.confirmUserName = confirmUserName;
	  }
});