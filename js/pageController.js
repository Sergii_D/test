app.controller('pageController', function($scope, validationService, cardFactory) {
      $scope.cardlist = cardFactory.getCards();
      $scope.formData = {};
      $scope.formData.contactAgreement = true;
      $scope.dialogIsHidden = true;

      $scope.submitContact = function(){          
            var emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            if(validationService.isValid($scope.formData.contactEmail,emailPattern)){
                  var data = {
                        email: $scope.formData.contactEmail,
                        password: $scope.formData.contactPassword,
                        message: $scope.formData.contactMessage
                  };
                  console.log(data);
                  $scope.dialogIsHidden = false;
                  $scope.messageToUser = "Your Message is sent. Thank you";
                  $scope.styleClass = "success";
                   $scope.formData = {};
            } else{
                  $scope.dialogIsHidden = false;
                  $scope.messageToUser = "Something wrong with input data";
                  $scope.styleClass = "danger";
            }            
      };

      $scope.hideDialog = function() {
          $scope.dialogIsHidden = true;
      };

      $scope.confirmUserName = function(){
            console.log("1");
            $scope.userNameConfirmed = true;
      };
      $scope.selectItem = function(index){
            $scope.singleCard = cardFactory.getSingleCard(index);
            $scope.singleCard.index = index;
      };
});